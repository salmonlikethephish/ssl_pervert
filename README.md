# SSL Pervert

> ## Not well tested. Not even close to production ready. Seriously.

Decrypt and forward HTTPS traffic to a remote server as if it were HTTP.
Useful for security analysis and monitoring of network traffic. Requests are 
rebuilt to appear as if the end was interacting with a HTTP server, replacing
references to protocols and ports where necessary.

SSL Pervert is a small ICAP server written in Python. Squid is used as the
Man-in-the-middle (MITM) SSL Bumping proxy.

This project provides a quick-start script ``go.sh`` and a puppet module to
build and configure a virtual machine.

## Requirements

- Install CentOS 6.x Minimal Latest (not tested in 7 yet)
- Configure two network interfaces with static IPs to start at boot time:
    - ``eth0`` To proxy traffic, inbound requests and outbound  
    - ``eth1`` To send fake decrypted SSL traffic

HTTP(S) requests coming into Squid are sent to our ICAP server for inspection
as decrypted payloads. We copy the payload, replace any references from HTTPS
to HTTP and build a fake sequence of packets.

Generated packets are dumped on to ``eth1``. They are not directed or forwarded.
You will need to put your monitoring appliance on the same subnet as ``eth1``
and use promiscuous mode to sniff the traffic.

## Installation 

One-liner (technically four but you are lazy.)
```
yum -y install git && cd /tmp && git clone https://gitlab.com/salmonlikethephish/ssl_pervert.git && ./go.sh
```

### Notes

- Use a dedicated VM. Running this script on an existing machine **will** cause
lots of fun side effects.
- We install a local version of puppet and run locally.

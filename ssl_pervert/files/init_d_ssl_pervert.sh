#!/bin/sh
# chkconfig: 123456 90 10
# SSL Pervert
#
workdir=/opt/ssl_pervert
 
start() {
    cd $workdir
    /usr/bin/python ssl_pervert.py 2>&1 /dev/null &
    pid=`ps -ef | pgrep -f 'ssl_pervert.py'`
    echo "SSL Pervert Started [$pid]"
}
 
status() {
    pid=`ps -ef | pgrep -f 'ssl_pervert.py'`
    if [ -z "$pid" ]; then
    	echo "SSL Pervert Not Running"
    else
    	echo "SSL Pervert Running [$pid]" 
    fi
}

stop() {
    pid=`ps -ef | pgrep -f 'ssl_pervert.py'`
    kill $pid
    echo "SSL Pervert Stopped"
}
 
case "$1" in
  start)
    start
    ;;
  status)
    status   
    ;;    
  stop)
    stop   
    ;;
  restart)
    stop
    start
    ;;
  *)
    echo "Usage: ssl_pervert {start|stop|restart}"
    exit 1
esac
exit 0
#!/bin/env python
# -*- coding: utf8 -*-

import random
import SocketServer

from pyicap import *
from scapy.all import *
from scapy_http import *

class ThreadingSimpleServer(SocketServer.ThreadingMixIn, ICAPServer):
    pass

class ICAPHandler(BaseICAPRequestHandler):

    def example_OPTIONS(self):
        self.set_icap_response(200)
        self.set_icap_header('Methods', 'REQMOD')
        self.set_icap_header('Service', 'PyICAP Server 1.0')
        self.send_headers(False)

    def example_REQMOD(self):        

        # Interface to put packets on to
        IFACE="eth1"

        # Source IP to pretend packets come from
        # TODO: Take from request X-Forward-For Header
        SRC_IP="192.168.1.149"


        sport=RandNum(1024,65535)
        dport = 80

    	dest_url = self.enc_req[1]
    	protocol = dest_url.find("://")
    	if( protocol > -1 ):
        	   start = protocol+ 3
    	else:
       	   start = 0

    	end = dest_url.find("/", start)
    	if(end == -1):
    	    end = len(dest_url)
    	
    	host = dest_url[start:end]
    	
    	port = host.find(":")
    	if port > -1:
    	    host = host[:port]

    	ip=IP(src=SRC_IP, dst=host)

        self.set_icap_response(200)

        # Build our request
        HTTP_REQUEST = ' '.join(self.enc_req) + '\r\n'

        self.set_enc_request(' '.join(self.enc_req))
        for h in self.enc_req_headers:
            for v in self.enc_req_headers[h]:
                self.set_enc_header(h, v)
                HTTP_REQUEST += h + ": " + v + '\r\n'

        # New line after the headers
        HTTP_REQUEST += '\r\n'

        REQ_BODY = ''

        # Nobody
        if not self.has_body:
            self.send_headers(False)
	
        # Preview only, need to get the full request
        elif self.preview:
            prevbuf = ''
            while True:
                chunk = self.read_chunk()
                if chunk == '':
                    break
                prevbuf += chunk
            REQ_BODY += prevbuf
            if self.ieof:
                self.send_headers(True)
                if len(prevbuf) > 0:
                    self.write_chunk(prevbuf)
                self.write_chunk('')
            else:
	         self.cont()
	         self.send_headers(True)
	         if len(prevbuf) > 0:
	             self.write_chunk(prevbuf)
	         while True:
	             chunk = self.read_chunk()
	             self.write_chunk(chunk)
	             if chunk == '':
	                 break
	             REQ_BODY += prevbuf

        # Full body available
        else:
            self.send_headers(True)
            while True:
                chunk = self.read_chunk()
                self.write_chunk(chunk)
                if chunk == '':
                    break
                REQ_BODY += chunk


        #HTTP_REQUEST += REQ_BODY

    	# Make headers look like HTTP
        HTTP_REQUEST = HTTP_REQUEST.replace("https://", "http://")
        HTTP_REQUEST = HTTP_REQUEST.replace(":443", ":80")

    	# Append the request body
    	HTTP_REQUEST += REQ_BODY

    	# Build a request so we can force the interface
        req=Ether()/ip/TCP(sport=sport, dport=80)/HTTP_REQUEST
        #req.show()

    	# If we will exceed MTU then fragment
    	if len(req) > 1480:
    	    req = fragment(req)
    	
            sendp( req, iface=IFACE, verbose=0)



# Listen on this port
port = 13440
server = ThreadingSimpleServer(('', port), ICAPHandler)
try:
    while 1:
        server.handle_request()
except KeyboardInterrupt:
    print "Finished"


class ssl_pervert::application {

    group { 'ssl_pervert':
          ensure => 'present',
          gid    => '503',
     }

     user { 'ssl_pervert':
       ensure   => 'present',
       gid      => '503',
       uid      => '502',
       subscribe => Group['ssl_pervert']
     }

    # Setup our scripts
    file { '/opt/ssl_pervert':
        ensure => 'directory',
        owner => 'ssl_pervert',
        group => 'ssl_pervert',
        subscribe => User['ssl_pervert']
    }

    file { '/opt/ssl_pervert/ssl_pervert.py':
        ensure => 'file',
        content => file('ssl_pervert/ssl_pervert.py'),
        mode => 500,
        owner => 'ssl_pervert',
        group => 'ssl_pervert',
        subscribe => File['/opt/ssl_pervert']     
    }

    file { '/opt/ssl_pervert/pyicap.py':
        ensure => 'file',
        content => file('ssl_pervert/pyicap.py'),
        mode => 500,
        owner => 'ssl_pervert',
        group => 'ssl_pervert',
        subscribe => File['/opt/ssl_pervert']        
    }

    file { '/opt/ssl_pervert/scapy_http.py':
        ensure => 'file',
        content => file('ssl_pervert/scapy_http.py'),
        mode => 500,
        owner => 'ssl_pervert',
        group => 'ssl_pervert',
        subscribe => File['/opt/ssl_pervert']          
    }

    # Setup ssl pervert init script
    file { '/etc/init.d/ssl_pervert':
        ensure => 'file',
        mode => 755,
        seltype => "initrc_exec_t",        
        content => file('ssl_pervert/init_d_ssl_pervert.sh'),
        owner => 'ssl_pervert',
        group => 'ssl_pervert'      
    }

    # Start SSL pervert at boot
    service { 'ssl_pervert':
      enable => true,
      ensure => 'running',
      subscribe => File['/etc/init.d/ssl_pervert']  
    }

}
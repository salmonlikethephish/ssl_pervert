class ssl_pervert::prereqs {
	
    # Install EPEL
    exec { 'Linux EPEL':
      unless => '/bin/ls /etc/yum.repos.d | /bin/grep epel.repo',
      command  => '/bin/rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm',
      logoutput => true,
    }

	# No support for groups yet so cheat
    exec { 'yum Group Install':
      unless  => '/usr/bin/yum grouplist "Development tools" | /bin/grep "^Installed Groups"',
      command => '/usr/bin/yum -y groupinstall "Development tools"',
    }

    $required_packages = [ 'openssl-devel', 'libxml2-devel', 'libcap-devel', 'tcpdump', 'python', 'wget', 'zip', 'unzip', 'ntp', 'python-pip']
    package { $required_packages:
      ensure  => installed,
    }

    service { 'ntpd':
      ensure  => running,
    }

    # Install Scapy
    exec { 'Linux Scapy':
      unless => '/bin/ls /usr/local/bin | /bin/grep scapy',
      command  => '/usr/bin/wget -O /usr/local/bin/scapy scapy.net && /bin/chmod +x /usr/local/bin/scapy',
      logoutput => true,
      cwd => '/tmp'
    }

    # Install python scapy
    exec { 'Python Scapy':
      command  => '/usr/bin/pip install scapy',
      logoutput => true,
      subscribe => Exec['Linux Scapy']
    }

    file { '/etc/selinux/config':
        ensure => 'file',
        content => file('ssl_pervert/selinux'),
        seluser => "system_u",
        selrole => "object_r",
        seltype => "selinux_config_t",
    }

    # Iptables
    file { '/etc/sysconfig/iptables':
        ensure => 'file',
        content => file('ssl_pervert/iptables'),
        notify  => Service['iptables']
    }

    service { 'iptables':
      ensure  => 'running',
      enable  => true,
    }

}
class ssl_pervert::squid {

    # Download Squid
    exec { 'Download Squid':
      unless => '/bin/ls /usr/sbin | /bin/grep squid',
      command  => '/usr/bin/wget -O squid-latest.tar.gz http://www.squid-cache.org/Versions/v3/3.5/squid-3.5.15.tar.gz && /bin/tar xfz squid-latest.tar.gz -C squid-latest --strip-components=1',
      cwd => '/tmp',
      logoutput => true,
    }

    exec { 'Configure Squid':
      unless => '/bin/ls /usr/sbin | /bin/grep squid',
      command  => file("ssl_pervert/configure"),
      cwd => '/tmp/squid-latest',
      logoutput => true,
      subscribe => Exec['Download Squid']
    }

    exec { 'Make Squid':
      unless => '/bin/ls /usr/sbin | /bin/grep squid',
      command  => '/usr/bin/make',
      cwd => '/tmp/squid-latest',
      timeout => 0,
      logoutput => true,
      subscribe => Exec['Configure Squid']      
    }

    exec { 'Install Squid':
      unless => '/bin/ls /usr/sbin | /bin/grep squid',
      command  => '/usr/bin/make install',
      cwd => '/tmp/squid-latest',
      logoutput => true,
      subscribe => Exec['Make Squid']
    }

    # Setup squid config
    file { '/etc/squid/squid.conf':
        ensure => 'file',
        content => file('ssl_pervert/squid.conf'),
        owner => 'squid',
        group => 'squid',
    }

    # Setup squid init script
    file { '/etc/init.d/squid':
        ensure => 'file',
        content => file('ssl_pervert/init_d_squid.sh'),
        mode => 755,
        seltype => "squid_initrc_exec_t",
        subscribe => Exec['Install Squid']         
    }

    # Start squid at boot
    service { 'squid':
      enable => true,
      ensure => 'running',
      subscribe => File['/etc/init.d/squid']
    }

    # Fix pinger permissions
    file { '/usr/lib64/squid/pinger':
      ensure => 'file',
      mode => 755,
      subscribe => Exec['Install Squid']      
    }

    # Create SSL certs
    exec { 'Create SSL Private Key':
      unless => '/bin/ls /etc/squid/ | /bin/grep *.private',
      command  => '/usr/bin/openssl genrsa -out example.com.private 2048',
      cwd => '/etc/squid/',
      logoutput => true,
    }

    # Create SSL certs
    exec { 'Create SSL CSR':
      unless => '/bin/ls /etc/squid/ | /bin/grep *.csr',
      command  => '/usr/bin/openssl req -new -key example.com.private -out example.com.csr -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=example.com"',
      cwd => '/etc/squid/',
      logoutput => true,
      subscribe => Exec['Create SSL Private Key']
    }

    # Create SSL certs
    exec { 'Create SSL CRT':
      unless => '/bin/ls /etc/squid/ | /bin/grep *.crt',
      command  => '/usr/bin/openssl x509 -req -days 3652 -in example.com.csr -signkey example.com.private -out example.com.crt',
      cwd => '/etc/squid/',
      logoutput => true,
      subscribe => Exec['Create SSL CSR']
    }

}
class ssl_pervert::squid_prereqs {

     group { 'squid':
          ensure => 'present',
          gid    => '502',
     }

     user { 'squid':
       ensure	=> 'present',
       gid		=> '502',
       uid		=> '501',
       subscribe => Group['squid']
     }

    file { '/var/log/squid':
        ensure => 'directory',
        owner => 'squid',
        group => 'squid',
        subscribe => User['squid']
    }  

    file { '/var/spool/squid':
        ensure => 'directory',
        owner => 'squid',
        group => 'squid',
        subscribe => User['squid']        
    }     

    file { '/var/spool/squid/cache':
        ensure => 'directory',
        owner => 'squid',
        group => 'squid',
        subscribe => User['squid']        
    }

    file { '/etc/squid':
        ensure => 'directory',
        owner => 'squid',
        group => 'squid',
        subscribe => User['squid']        
    }

 	file { '/tmp/squid-latest':
        ensure => 'directory',
    }

}